package com.folcademy.banco_fonzalida;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoFonzalidaApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoFonzalidaApplication.class, args);
	}

}
