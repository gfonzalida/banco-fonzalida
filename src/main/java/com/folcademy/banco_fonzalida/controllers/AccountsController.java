package com.folcademy.banco_fonzalida.controllers;

import com.folcademy.banco_fonzalida.models.entities.AccountEntity;
import com.folcademy.banco_fonzalida.models.repositories.AccountRepository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountsController {

    private final AccountRepository accountRepository;

    public AccountsController(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @GetMapping("/{userId}")
    public List<AccountEntity> getUserAccounts(@PathVariable String userId) {
        return accountRepository.findAllByUserId(userId);
    }

    @PostMapping("/new")
    public AccountEntity newAccount(@RequestBody AccountEntity accountEntity){
        return accountRepository.save(accountEntity);
    }
}

