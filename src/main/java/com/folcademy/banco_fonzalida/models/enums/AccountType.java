package com.folcademy.banco_fonzalida.models.enums;

public enum AccountType {
    CUENTA_CORRIENTE, CAJA_DE_AHORROS
}
