package com.folcademy.banco_fonzalida.models.repositories;

import com.folcademy.banco_fonzalida.models.entities.AccountEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccountRepository extends CrudRepository<AccountEntity, Long> {
    List<AccountEntity> findAllByUserId(String userId);

}
